/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 */
describe( 'home section', function() {
  beforeEach( module( 'ServiceDashboard.home' ) );

  it( 'should inject the charts into the view', inject( function($controller) {
    var scope = {},
      ctrl = $controller('HomeCtrl', { $scope: scope });
      
    expect(scope.charts.length).toBe(2);
  }));

  it( 'should have a dummy test', inject( function() {
    expect( true ).toBeTruthy();
  }));
});

