angular.module( 'ServiceDashboard.responseStatus', [
  'ui.state',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'responseStatus', {
    url: '/responseStatus',
    views: {
      "main": {
        controller: 'ResponseStatusCtrl',
        templateUrl: 'responseStatus/responseStatus.tpl.html'
      }
    },
    data:{ pageTitle: 'Response Status Codes' }
  });
})

.controller( 'ResponseStatusCtrl', function ResponseStatusCtrl( $scope ) {
  // This is simple a demo for UI Boostrap.
  $scope.dropdownDemoItems = [
    "The first choice!",
    "And another choice for you.",
    "but wait! A third!"
  ];
})

;
