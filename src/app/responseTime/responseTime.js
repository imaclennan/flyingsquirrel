angular.module( 'ServiceDashboard.responseTime', [
  'ui.state',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'responseTime', {
    url: '/responseTime',
    views: {
      "main": {
        controller: 'ResponseTimeCtrl',
        templateUrl: 'responseTime/responseTime.tpl.html'
      }
    },
    data:{ pageTitle: 'Response Times' }
  });
})

.controller( 'ResponseTimeCtrl', function ResponseTimeCtrl( $scope ) {
  // This is simple a demo for UI Boostrap.
  $scope.dropdownDemoItems = [
    "The first choice!",
    "And another choice for you.",
    "but wait! A third!"
  ];
})

;
