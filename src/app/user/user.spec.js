/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 */
describe( 'User package', function() {
  beforeEach( module( 'ServiceDashboard.user' ) );

  it( 'should get the current authenticated state from the model', inject( function($controller) {
    var scope = {},
      user = {
        getUsername: function() {
          return 'testUserName';
        },
        isAuthenticated: function() {
          return true;
        }
      },
      ctrl = $controller('UserCtrl', { $scope: scope, User: user });
      
    expect(scope.username).toBe('testUserName');
    expect(scope.isAuthenticated).toBe(true);
  }));

  it( 'should try to authenticate when the authenticate function is called', inject( function($controller, User) {
    var scope = {},
      ctrl = $controller('UserCtrl', { $scope: scope, User: User });

    scope.username = 'user1';
    scope.password = 'pass1';

    getUsernameSpy = spyOn(User, 'getUsername').andReturn('user1');
    isAuthenticatedSpy = spyOn(User, 'isAuthenticated').andReturn(true);
    authenticateSpy = spyOn(User, 'authenticate').andCallFake(function (username, password, callback) {
      callback(true);
    });

    scope.authenticate();

    expect(authenticateSpy).toHaveBeenCalledWith('user1', 'pass1', jasmine.any(Function));
    expect(isAuthenticatedSpy).toHaveBeenCalled();
    expect(getUsernameSpy).toHaveBeenCalled();

    expect(scope.username).toBe('user1');
    expect(scope.isAuthenticated).toBe(true);
  }));

  it( 'should try to logout when the logout function is called', inject( function($controller, User) {
    var scope = {},
      ctrl = $controller('UserCtrl', { $scope: scope, User: User });

    scope.username = 'user1';
    scope.password = 'pass1';

    getUsernameSpy = spyOn(User, 'getUsername').andReturn(null);
    isAuthenticatedSpy = spyOn(User, 'isAuthenticated').andReturn(false);
    logoutSpy = spyOn(User, 'logout').andCallFake(function (callback) {
      callback(true);
    });

    scope.logout();

    expect(logoutSpy).toHaveBeenCalledWith(jasmine.any(Function));
    expect(isAuthenticatedSpy).toHaveBeenCalled();
    expect(getUsernameSpy).toHaveBeenCalled();

    expect(scope.username).toBe(null);
    expect(scope.isAuthenticated).toBe(false);
  }));
});

describe( 'User service', function() {
  var src,
    httpBackend;

  beforeEach(function (){
    module('ServiceDashboard.user');

    inject(function($httpBackend, User) {
      svc = User;
      httpBackend = $httpBackend;
    });
  });

  afterEach(function (){
    httpBackend.verifyNoOutstandingExpectation();
    httpBackend.verifyNoOutstandingRequest();
  });

  describe('Successful login', function() {
    it('should call the login service when a login attempt is made.', function() {
      var successData = {
        status: 200,
        username: 'jimbBob',
        token: 'abc123'
      };

      httpBackend.expectPOST('/api/login', { username: 'jimbBob', password: 'fooPassword'}).respond(successData);

      var callback = jasmine.createSpy();

      result = svc.authenticate('jimbBob', 'fooPassword', callback);

      httpBackend.flush();

      expect(callback).toHaveBeenCalled();
      expect(svc.username).toBe('jimbBob');
      expect(svc.authenticated).toBe(true);
      expect(svc.authToken).toBe('abc123');
    });
  });

  describe('Failed login', function() {
    it('should call the login service when a login attempt is made and respect a failure.', function() {
      var failData = {
        status: 404,
        username: null,
        token: null
      };

      httpBackend.expectPOST('/api/login', { username: 'jimbBob', password: 'fooPassword'}).respond(failData);

      var callback = jasmine.createSpy();

      result = svc.authenticate('jimbBob', 'fooPassword', callback);

      httpBackend.flush();

      expect(callback).toHaveBeenCalled();
      expect(svc.username).toBe(null);
      expect(svc.authenticated).toBe(false);
      expect(svc.authToken).toBe(null);
    });
  });
});