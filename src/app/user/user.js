angular.module( 'ServiceDashboard.user', [
  'ui.state',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'user', {
    url: '/user',
    views: {
      "main": {
        controller: 'UserCtrl',
        templateUrl: 'user/user.tpl.html'
      }
    },
    data:{ pageTitle: 'User Profile' }
  });

  $stateProvider.state( 'register', {
    url: '/register',
    views: {
      "main": {
        controller: 'RegisterCtrl',
        templateUrl: 'user/register.tpl.html'
      }
    },
    data:{ pageTitle: 'Register' }
  });
})

.controller( 'RegisterCtrl', function RegisterCtrl( $scope, User ) {
  $scope.user = {};

  $scope.register = function() {
    User.create(user, function(err, user) {

    });
  };
})

.controller( 'UserCtrl', function UserCtrl( $scope, User ) {
  $scope.isAuthenticated = User.isAuthenticated();

  $scope.username = User.getUsername();

  $scope.password = null;

  $scope.authenticate = function() {
    User.authenticate($scope.username, $scope.password, function(result) {
      if (result) {
        $scope.isAuthenticated = User.isAuthenticated();
        $scope.username = User.getUsername();
        $scope.password = '';
      }
    });
  };

  $scope.logout = function() {
    User.logout(function(result) {
      if (result) {
        $scope.isAuthenticated = User.isAuthenticated();
        $scope.username = User.getUsername();
      }
    });
  };
})

.factory( 'User', ['$http', function($http) {
  return {
    authenticated: false,

    username: null,

    authToken: null,

    getUsername: function() {
      return this.username;
    },

    isAuthenticated: function() {
      return this.authenticated;
    },

    logout: function(callback) {
      this.authenticated = false;
      this.username = null;
      this.authToken = null;

      callback();
    },

    create: function(user, callback) {
      return({
        method: 'POST',
        url: '/api/users',
        date: user,
        header: {
          'Content-Type': 'application/json'
        }
      }).then(function (result) {
        if (result.status == 201) {
          callback(null, result.data);
        } else {
          callback({
            'message': 'Could not create user'
          });
        }
      });
    },

    authenticate: function(username, password, callback) {
      var userObject = this;

      return $http({
        method: 'POST',
        url: '/api/login',
        data: {
          'username': username,
          'password': password
        },
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(function (result) {
        if (result.data.status == 200) {
          userObject.username = result.data.username;
          userObject.authToken = result.data.token;
          userObject.authenticated = true;
          callback(true);
        } else {
          userObject.username = null;
          userObject.authToken = null;
          userObject.authenticated = false;
          callback(false);
        }
      });
    }
  };
}])

;
